import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    memberName: '',
    memberAge: 0,
}

const memberSlice = createSlice({
    name: 'member',
    initialState,
    reducers: {
        patchMemberName: (state, action) => {
           // const { name } = action.payload // { name : '홍길동' }
           // state.memberName = name 값이 여러개일때 쓴다.
            state.memberName = action.payload // 값을 하나만 줄때 쓴다.
        },
        patchMemberAge: (state, action) => {
            state.memberAge = action.payload
        }
    }
})

export const { patchMemberName, patchMemberAge} = memberSlice.actions
export default memberSlice.reducer