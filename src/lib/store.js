import { configureStore } from '@reduxjs/toolkit'
import memberReducer from "@/lib/features/member/memberSlice";

export default configureStore({
    reducer: {
        member: memberReducer
    },
})