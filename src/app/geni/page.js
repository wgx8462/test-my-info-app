'use client'
import {useSelector} from "react-redux";
import Link from "next/link";

export default function Geni() {
    const {memberName} = useSelector((state) => state.member);

    return (
        <div>
            <div>{memberName} 당신은 천재입니다.</div>
            <Link href="/age">나이 입력하기</Link>
        </div>
    )
}