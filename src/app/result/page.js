'use client'

import { useSelector } from "react-redux";

export default function Result() {
    const { memberName, memberAge } = useSelector((state) => state.member)

    return (
        <div>
            이름 : { memberName } <br/>
            나이 : { memberAge }
        </div>
    )
}