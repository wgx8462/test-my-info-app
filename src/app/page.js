'use client'
import {useState} from "react";
import {useSelector, useDispatch} from "react-redux";
import {patchMemberName} from "@/lib/features/member/memberSlice";
import Link from "next/link";

export default function Home() {
    const [memberName, setMemberName] = useState('')
    const dispatch = useDispatch()

    const handleSaveName = () => {
        dispatch(patchMemberName(memberName))
    }
    return (
        <main>
            <input type="text" value={memberName} onChange={(e) => setMemberName(e.target.value)}/>
            <button onClick={handleSaveName}>저장</button>
            <Link href="/geni">이름확인</Link>
        </main>
    );
}
