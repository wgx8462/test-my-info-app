'use client'

import { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import Link from "next/link";
import { patchMemberAge } from "@/lib/features/member/memberSlice";

export default function Age() {
    const [age, setAge] = useState('')
    const {memberName} = useSelector((state) => state.member)
    const dispatch = useDispatch()

    const handleAgeSave = () => {
        dispatch(patchMemberAge(Number(age)))
    }

    return (
        <div>
            {memberName} 님 나이를 입력해주세요. <br/>
            <input type="number" value={age} onChange={(e) => setAge(e.target.value)} />
            <button onClick={handleAgeSave}>저장</button>

            <br/>
            <Link href="/result">확인</Link>
        </div>
    )
}